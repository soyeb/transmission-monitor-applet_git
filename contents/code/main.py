# -*- coding: utf-8 -*-
# Copyright 2012 Soyeb Aswat
# Licensed under the terms of the GNU Pubilc License v2

from PyQt4.QtCore import Qt
from PyQt4.QtCore import QTimer
from PyQt4.QtGui  import QGraphicsLinearLayout
from PyQt4 import QtCore, QtGui

from PyKDE4.plasma import Plasma
from PyKDE4 import plasmascript

import transmissionrpc
 
#==============================================================================

class TransmissionMonitorApplet(plasmascript.Applet):

    def __init__(self,parent,args=None):
        # call parent constructor
        plasmascript.Applet.__init__(self,parent)
 
    #==========================================================================

    def init(self):
        # basic config
        self.setHasConfigurationInterface(False)
        self.setAspectRatioMode(Plasma.Square)

        # set background
        self.theme = Plasma.Svg(self)
        self.theme.setImagePath("widgets/background")
        self.setBackgroundHints(Plasma.Applet.DefaultBackground)

        # prepare gui and layout
        self.layout = QGraphicsLinearLayout(Qt.Vertical, self.applet)
        self._label = Plasma.Label(self.applet)
        self._label.setText("Initialising.")
        self._butt = Plasma.PushButton()
        self._butt.setText("Turtle")
        self._butt.setCheckable(True)
        self._butt.setChecked(False)
        QtCore.QObject.connect(self._butt, QtCore.SIGNAL("clicked()"), self._buttClicked)
        self.layout.addItem(self._label)
        self.layout.addItem(self._butt)
        self.applet.setLayout(self.layout)
        #self.resize(150,150)

        # create transmission connection object
        self._connectToTransmission()

        # setup display update timer
        self._updateTimer = QTimer()
        self._updateTimer.setSingleShot(False)
        QtCore.QObject.connect(self._updateTimer, QtCore.SIGNAL("timeout()"), self._updateDisplay)
        #QtCore.QMetaObject.connectSlotsByName(self)
        self._updateTimer.start(1000)

    #==========================================================================

    def _connectToTransmission(self) :
        try:
            self._tc = transmissionrpc.Client('localhost', port=9091)
        except Exception :
            self._tc = None
            self._label.setText("Start Transmission!")

    #==========================================================================

    # attempt to connect if not connected, and return connection or None
    def _getConnection(self) :
        if self._tc is None :
            self._connectToTransmission()
        return self._tc

    #==========================================================================

    def _updateDisplay(self) :
        # if connected get stats
        if self._getConnection() is not None :
            try :
                stats = self._tc.session_stats()
                #self._label.setText( "D: "+str(stats.downloadSpeed/1024)+" kB/s   U: "+str(stats.uploadSpeed/1024)+" kB/s" )
                self._label.setText( "D: "+str(stats.downloadSpeed/1024) )
                self._butt.setChecked( self._tc.get_session().alt_speed_enabled )
            except Exception :
                self._tc = None
                self._label.setText("Start Transmission!")

    #==========================================================================

    def _buttClicked(self) :
        if self._getConnection() is not None :
            buttStatus =  self._butt.isChecked()
            try :
                self._tc.set_session( alt_speed_enabled=buttStatus )
            except Exception :
                self._tc = None
                self._label.setText("Start Transmission!")

#==============================================================================

def CreateApplet(parent):
    return TransmissionMonitorApplet(parent)

